import React from 'react';

class API {
    /* fetchToken: () => {
        *const [newToken, setNewToken] = useContext(TokenStore);
         const [isLoading, setIsLoading] = useState(true)
         useEffect(() => {
             fetch(`${SERVER_IP}/auth/token`)
                 .then(response => response.text())
                 .then(token => {
                     setIsLoading(true), setNewToken(token)
                 })
                 .catch(error => console.log("error", error))
                 .finally(() => setIsLoading(false))
         }, [])
         return isLoading;
    },*/
    keycloak: any;

    constructor(keycloak: any) {
        this.keycloak = keycloak
    }

    // change all localhost to our port in AWS if we want to start global
    // with http://16.170.204.176:3000


    static fetchAllCompanies = () => {
        return fetch("http://localhost:8080/company/all-approved-companies")
            .then(resp => resp.json())
    }

    fetchAllApprovedSchools = () => {
        return fetch("http://localhost:8080/school/all-approved-schools", {
            method: 'GET',
            headers: {"Authorization": "Bearer " + this.keycloak.token}
        })
            .then(resp => resp.json())
    }

    getApprovedSchoolsById = (id: string) => {
        return fetch("http://localhost:8080/school/get-approve-school-by-id/" + id, {
            method: 'GET',
            headers: {"Authorization": "Bearer " + this.keycloak.token}
        })
            .then(resp => resp.json())
    }

    fetchAllSchoolsRequest = () => {
        if (!this.keycloak) {
            console.log("inte fins")
            throw "inte fins"

        }
        return fetch("http://localhost:8080/admin/all-school-requests", {
            method: 'GET',
            headers: {"Authorization": "Bearer " + this.keycloak.token}
        })
            .then(resp => resp.json())
    }

    deleteRequestSchool = (id: string) => {
        return fetch("http://localhost:8080/admin/delete-school-requests?schoolId=" + id, {
            method: "DELETE",
            headers: {"Authorization": "Bearer " + this.keycloak.token}
        });
    };

    deleteApproveSchool = async (id: string) => {
        await fetch("http://localhost:8080/school/remove-school/" + id, {
            method: "DELETE",
            headers: {"Authorization": "Bearer " + this.keycloak.token}
        });

    };

    approveSchool = async (id: string) => {
        await fetch("http://localhost:8080/admin/approve-school-requests?schoolId=" + id, {
            method: "POST",
            headers: {"Authorization": "Bearer " + this.keycloak.token}
        })
            .then(r => r.json())
    };

    fetchAllStudents = () => {
        return fetch("http://localhost:8080/student/all")
            .then(resp => resp.json())
    }

    fetchGetAdminById = () => {
        if (!this.keycloak) {
            console.log("inte fins")
            throw "inte fins"
        }
        return fetch("http://localhost:8080/admin/get_By_Id", {
            method: 'GET',
            headers: {"Authorization": "Bearer " + this.keycloak.token}
        })
            .then(resp => resp.json())
    }

    fetchAllAdmins() {
        return fetch("http://localhost:8080/admin/all", {
            method: 'GET',
            headers: {"Authorization": "Bearer " + this.keycloak.token}
        })
            .then(resp => resp.json())
    }
}

export default API;
