import React from 'react';
import {BrowserRouter, Route, Switch} from "react-router-dom";
import MyPagesHome from "../views/MyPagesHome";
// import LoggaInPage from "../views/LoggaInPage";
import MyProfileInfo from "../components/minaSidorComponents/myPagesInfo/MyProfileInfo";
import LandingPage from "../views/LandingPage";
import EditMyProfile from "../components/minaSidorComponents/myPagesInfo/EditMyProfile";
import SchoolProfile from "../components/schoolProfile/SchoolProfile";


const Routes = () => {
    return (
        <Switch>
            <Route path="/admin/MyPagesHome" component={MyPagesHome}/>
            {/*<Route path="/admin/LoggaInPage" component={LoggaInPage}/>*/}
            <Route path="/admin/minaSidor" component={MyPagesHome}/>
            <Route path="/admin/minaSidor/MyProfileInfo" component={MyProfileInfo}/>
            <Route path="/admin/minaSidor/MyStudnets" component={MyProfileInfo}/>
            <Route path="/admin/minaSidor/skolor" component={MyProfileInfo}/>
            <Route path="/admin/minaSidor/skolor/skolProfil" component={SchoolProfile}/>
         {/*   <Route path="/admin/minaSidor/skolor/skolProfilEdit" component={EditSchoolProfile}/>*/}
            <Route path="/" component={LandingPage}/>
            <Route path="/admin/MyPagesHome/MyProfileInfo/edit" component={EditMyProfile}/>
        </Switch>

    );
};

export default Routes;
