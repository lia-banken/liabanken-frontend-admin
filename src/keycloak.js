import Keycloak from 'keycloak-js'

// Setup Keycloak instance as needed ff
// Pass initialization options as required or leave blank to load from 'keycloak.json jhyfiyfg'
const keycloak = Keycloak({
    url: 'http://localhost:8000/auth',
    realm: 'test',
    clientId: 'test-student',
});



export default keycloak
