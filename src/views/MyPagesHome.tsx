import React from 'react';
import {makeStyles} from "@material-ui/styles";
import MyPagesSelection from "../components/minaSidorComponents/MyPagesSelection";

const useStyles = makeStyles(() => ({
    root: {
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center"
    }
}));
const MyPagesHome: React.FC = () => {
    const classes = useStyles();
    return (
        <div className={classes.root}>
            <MyPagesSelection/>
        </div>
    );
}

export default MyPagesHome;
