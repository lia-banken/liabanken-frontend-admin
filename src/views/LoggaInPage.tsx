// import React from 'react';
import Article from "../components/landingPage/Article";
import {Button, makeStyles} from "@material-ui/core";
import {TextField} from "@mui/material";
// import HomePage from "../homePage/HomePage";
import {useHistory} from "react-router-dom";
import {Link} from 'react-router-dom';


// const useStyles = makeStyles(() => ({
//     title: {
//         textAlign: 'center',
//         fontSize: '30px',
//         fontWeight: 'bold',
//         marginTop: '12px',
//         marginBottom: '12px'
//     },
//
//     loginBody: {
//         display: 'flex',
//         backgroundColor: '#081992',
//         justifyContent: 'center',
//         "@media (min-width: 650px)": {
//             width: "500px",
//             marginLeft: "auto",
//             marginRight: "auto",
//         },
//         "@media (max-width: 650px)": {
//             maxWidth: "90%",
//             margin: "0 auto"
//         }
//     },
//     loginForm: {
//         display: 'flex',
//         flexDirection: 'column',
//         width: '90%',
//         marginTop: '40px',
//         marginBottom: '40px',
//     },
//
//     usernameSection: {
//         marginBottom: '40px'
//     },
//
//     passwordSection: {
//         marginBottom: '40px'
//     },
//
//     textLabel: {
//         color: 'white',
//         marginBottom: '7px'
//     },
//     input: {
//         width: '100%',
//         backgroundColor: "white"
//     },
//     bottomItem: {},
//     buttonSubmit: {
//         backgroundColor: '#238636',
//         width: '100%',
//         height: '50px',
//         fontSize: '20px',
//         fontWeight: 'bold',
//         '&:hover': {backgroundColor: '#34cb52',}
//     },
//     forgot: {
//         textAlign: 'end',
//         marginTop: '27px',
//         fontStyle: '#7D18FD',
//
//     },
//     articleContainer: {
//         marginTop: "30px",
//         marginBottom: "30px",
//         display: "flex",
//         flexDirection: "column",
//         "@media (min-width: 1200px)": {
//             flexDirection: "row",
//             justifyContent: "space-around",
//         },
//     }
// }));
//
//
// const LogInPage: React.FC = () => {
//     const classes = useStyles();
//     const history = useHistory()
//
//     /*  const routSubmit = () => {
//           let path = "/admin/minaSidor"
//         history.push(path)
//       }*/
//     return (
//
//         <div>
//             <div className={classes.title}>Log in</div>
//             <div className={classes.loginBody}>
//                 <form className={classes.loginForm}>
//                     <div className={classes.usernameSection}>
//                         <div className={classes.textLabel}>Username or email address</div>
//                         <div>
//                             <TextField
//                                 id="outlined-basic"
//                                 label="Username"
//                                 variant="filled"
//                                 color={"success"}
//                                 className={classes.input}
//                             />
//                         </div>
//
//                     </div>
//
//                     <div className={classes.passwordSection}>
//                         <div className={classes.textLabel}>Enter Password</div>
//                         <TextField
//                             id="outlined-basic"
//                             label="Password"
//                             variant="filled"
//                             color={"success"}
//                             className={classes.input}
//                         />
//                     </div>
//
//                     <div className={classes.bottomItem}>
//                         <Button component={Link} to="/admin/minaSidor"
//                                 className={classes.buttonSubmit}
//                                 data-testid="singIn"
//                         >Sign In</Button>
//                     </div>
//                     <div className={classes.forgot}>
//                         <Link to="/" style={{color: '#7D18FD'}}>forgot password?</Link>
//                     </div>
//                 </form>
//             </div>
//             <div className={classes.articleContainer}>
//                 <Article titleText={"Så hittar du lia"} text={"Guide för hur du hittar lia"}/>
//                 <Article titleText={"Skapa CV"} text={"Guide för hur du skapar CV"}/>
//                 <Article titleText={"Arbeta i Sverige"}
//                          text={"Rättigheter, skyldigheter"}/>
//             </div>
//         </div>
//     );
// };
//
// export default LogInPage;
