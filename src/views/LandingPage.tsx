import React from 'react';
import logo from '../assets/Logo.png';
import {makeStyles} from "@material-ui/core";
import Article from "../components/landingPage/Article";
import {Link, useHistory} from "react-router-dom";
import {Button, Divider} from "@mui/material";

const useStyles = makeStyles(() => ({
    landing: {
        paddingBottom: "3%",
        width: "100%",
    },
    landingLogo: {
        display: "flex",
        justifyContent: "center"
    },
    logo: {
        width: 300,
        height: 100,
        margin: "30px",
        "@media (max-width: 650px)": {
            width: "250px"
        },
    },
    welcomeContainer: {
        padding: "3%",
        textAlign: "center"
    },
    intrestedButtonContainer: {
        display: "flex",
        justifyContent: "center",
        padding: "3%",
        margin: "30px",
    },
    intrestedButton: {
        borderRadius: "30px",
    },
    articleContainer: {
        display: "flex",
        flexDirection: "column",
        "@media (min-width: 1200px)": {
            flexDirection: "row",
            justifyContent: "space-around",
        },
    }
}));
const Landing: React.FC = () => {
    const classes = useStyles();
    const history = useHistory()
    const aboutLiabanken: string = "Portalen är skapat med tanken på alla elever som har svårt att" +
        " hitta sin Lia plats. Den här portalenär framtagen för att underlätta alla dessa elever." +
        "  Tveka inte du som är skola eller ett företag att anmäla intresset!"


    return (
        <div className={classes.landing}>
            <div className={classes.landingLogo}>
                <img data-testid="logoImg"
                     src={logo} alt={"ggg"} className={classes.logo}/>
            </div>
            <div className={classes.welcomeContainer}>
                <h3 data-testid="welcomeText">Välkommen till LiaBanken</h3>
                <p>{aboutLiabanken}</p>
            </div>
            <div className={classes.intrestedButtonContainer}>
                <Button
                    data-testid="intresseAnmalan"
                    component={Link} to="/admin/minaSidor"
                    className={classes.intrestedButton}
                    variant="contained">Intresseanmälan
                </Button>
            </div>
            <div className={classes.articleContainer}>
                <Article titleText={"Så hittar du lia"} text={"Guide för hur du hittar lia"}/>
                <Article titleText={"Skapa CV"} text={"Guide för hur du skapar CV"}/>
                <Article titleText={"Arbeta i Sverige"} text={"Rättigheter, skyldigheter"}/>
            </div>
        </div>
    );
};

export default Landing;
