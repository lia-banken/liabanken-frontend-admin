import React from 'react';
import {fireEvent, render, screen} from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import LandingPage from "../views/LandingPage";
import {useHistory} from "react-router-dom";
import {BrowserRouter as Router} from 'react-router-dom';
import App from "../App";


const mockHistoryPush = jest.fn();

jest.mock('react-router-dom', () => ({
    ...jest.requireActual('react-router-dom'),
    useHistory: () => ({
        push: mockHistoryPush,
    }),
}));


test('test_button-intresseanmälan', () => {
    render(<Router><LandingPage/></Router>);
    //const menuBtn = screen.getByText("Intresseanmälan")
    const intresseBtn = screen.getByTestId("intresseAnmalan")

    expect(intresseBtn).toBeVisible();
    expect(intresseBtn).toBeInTheDocument();
    expect(intresseBtn).toBeEnabled();
});

test('test_logo-landing', () => {
    render(<LandingPage/>);
    const linkElement = screen.getByTestId("logoImg");
    expect(linkElement).toBeVisible();
    expect(linkElement).toBeInTheDocument();
    expect(linkElement).toBeEnabled();
});

test('renders in landingpage with correct navigation button', () => {

    render(<Router><LandingPage/></Router>);
    const intresseBtn = screen.getByTestId("intresseAnmalan")

    fireEvent.click(intresseBtn)
    /*const history = useHistory()*/
    //const route = '/admin/LoggaInPage';
    const signInBtn = screen.getByTestId("singIn");
    expect(signInBtn).toBeVisible();

});
