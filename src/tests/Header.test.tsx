import React from 'react';
import {render, screen} from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import Header from "../components/header/Header";
import SearchBar from "../components/header/SearchBar";
import RightMenu from "../components/header/RightMenu";

test('test_button_LoggaIn', () => {
    render(<Header/>);
    const linkElement = screen.getByText("Logga in");
    expect(linkElement).toBeVisible();
    expect(linkElement).toBeInTheDocument();
    expect(linkElement).toBeEnabled();
});

test('test_search_field', () => {
    render(<SearchBar/>);
    const linkElement = screen.getByPlaceholderText("Search…");
    expect(linkElement).toBeVisible();
    expect(linkElement).toBeInTheDocument();
    expect(linkElement).toBeEnabled();
});

test('test_menu', () => {
    render(<RightMenu/>);
    const menuBtn = screen.getByTestId("menuBtn")
    expect(menuBtn).toBeVisible();
    expect(menuBtn).toBeInTheDocument();
    expect(menuBtn).toBeEnabled();
});

