import React from 'react';
import {fireEvent, render, screen} from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import Footer from "../components/footer/Footer";

test('test_text_footer', () => {
    render(<Footer/>);
    const linkElement = screen.getByText("Other Languages");
    expect(linkElement).toBeVisible();
    expect(linkElement).toBeInTheDocument();
    expect(linkElement).toBeEnabled();
});

test('test_startIcon', () => {
    render(<Footer/>);
    const linkElement = screen.getByTestId("startIcon");
    expect(linkElement).toBeVisible();
    expect(linkElement).toBeInTheDocument();
    expect(linkElement).toBeEnabled();
});

