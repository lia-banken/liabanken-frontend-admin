import React, {FC} from "react";
import {Link, makeStyles} from "@material-ui/core";
import Divider from '@mui/material/Divider';
import ChangePasswordModal from "./modalEditAdmin/ChangePasswordModal";
import DeleteAccountModal from "./modalEditAdmin/DeleteAccountModal";

const useStyles = makeStyles(() => ({
    container: {
        display: "center",
        justifyContent: "center",
        alignItems: "center",
        flexDirection: "column",
        width: "100%",
        padding: 0,
        margin: 0
    },
    password: {
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        flexDirection: "column",
        margin:"50px"
    },
    account:{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        flexDirection: "column",
    },
    changeLink:{
        '&:hover':{ color:"blue"}
    },
    deleteLink:{
        '&:hover':{ color:"red"}
    }
}));
const Sittings: FC = () => {
    const classes = useStyles();
    return (
        <div className={classes.container}>
            <h3>Konto Inställningar</h3>
            <Divider/>
            <div className={classes.password}>
                <h4>ändra Lösenord</h4>
                <ChangePasswordModal/>
            </div>
            <Divider/>
            <div className={classes.account}>
                <h4>Ta bort konto</h4>
                <p>om du ta bort konto kommer du inte komma vara med tyvärr</p>
                <DeleteAccountModal/>
                {/*<Link href="#" className={classes.deleteLink}>Ta bort konto</Link>*/}
            </div>

        </div>
    );
}
export default Sittings;