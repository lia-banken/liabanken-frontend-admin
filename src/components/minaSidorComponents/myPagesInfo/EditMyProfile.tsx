import * as React from 'react';

import {FC, useState} from "react";
import {makeStyles} from "@material-ui/styles";
import {Box, TextField} from "@mui/material";
import {Button} from "@material-ui/core";
import MyProfileInfo from "./MyProfileInfo";
import ArrowBackIosIcon from "@mui/icons-material/ArrowBackIos";

const useStyles = makeStyles(() => ({
    root: {
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center",
    },
    oneRow:{
        margin:"20px",

    },
    buttonContainer: {
    display: "flex",
        justifyContent: "center",
        padding: "3%"
},
button: {
    color: "#efeaea",
        backgroundColor: "#081992",
        borderRadius: "30px",
        margin: "30px"
},
    buttonBack: {
        color: "#efeaea",
        backgroundColor: "#081992",
        borderRadius: "30px",
        margin: "30px"
    },
}));
const EditMyProfile: FC = () => {
    const classes = useStyles();
    const [profileInfoShow, setProfileInfoShow] = useState(false)
    const [editProfileInfoShow, setEditProfileInfoShow] = useState(true)
    const onClicked = () => {
        setProfileInfoShow(true)
        setEditProfileInfoShow(false)
    }

    const onClickedBack = () => {
        setProfileInfoShow(true)
        setEditProfileInfoShow(false)
    }
    const editProfileInfo = () => {

        return (
            <Box
                component="form"
                sx={{
                    '& .MuiTextField-root': {m: 1, width: '25ch'},
                }}
                noValidate
                autoComplete="off"
            >

                <div className={classes.root}>
                    <Button className={classes.buttonBack}
                            variant="contained"
                            startIcon={<ArrowBackIosIcon/>}
                            onClick={onClickedBack}

                    >
                        Tillbaka</Button>
                    <div className={classes.oneRow}>
                        <TextField id="outlined-basic" label="Namn" variant="outlined"/>
                        <TextField id="outlined-basic" label="EfterNamn" variant="outlined"/>

                    </div>
                    <div className={classes.oneRow}>
                        <TextField id="outlined-basic" label="E-mail" variant="outlined"/>
                        <TextField id="outlined-basic" label="Telefonnummer" variant="outlined"/>
                    </div>
                    <div className={classes.oneRow}>
                        <TextField id="outlined-basic" label="c/o" variant="outlined"/>
                        <TextField id="outlined-basic" label="HemAdress" variant="outlined"/>
                    </div>
                    <div className={classes.oneRow}>
                        <TextField id="outlined-basic" label="Postort" variant="outlined"/>
                        <TextField id="outlined-basic" label="Land" variant="outlined"/>
                    </div>

                    <div className={classes.buttonContainer}>
                        <Button
                            className={classes.button}
                            onClick={onClicked}
                            variant={"contained"}
                        >Spara mina uppgifter</Button>
                    </div>
                </div>
            </Box>

        );
    }

    return (
        <div className={classes.root}>
       {/*     {
                profileInfoShow ? <MyProfileInfo  /> : null
            }
            {
                editProfileInfoShow ? editProfileInfo() : null
            }*/}
        </div>
    );
}


export default EditMyProfile;
