import * as React from 'react';
import {FC, useContext, useEffect, useState} from "react";
import {Button, makeStyles, Paper} from "@material-ui/core";
import EditMyProfile from "./EditMyProfile";
import API from "../../../route/API";
import {AdminApiContext} from "../../../App";
import {IAdminItem} from "../../listComponents/administraterList/AdminListItem";
import {IPropsAdmin, IPropsAdminList} from "../../listComponents/administraterList/AdminstreraAdmin";


const useStyles = makeStyles(() => ({
    root: {
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center",
    },
    h1:{
        "@media (max-width: 650px)": {
            fontSize:"18px"

        }
    },
    row: {
        display: "flex",
        flexDirection: "row",
        justifyContent: "space-between",
        margin: "30px"


    },
    column: {
        display: "flex",
        flexDirection: "column",
        justifyContent: "space-between",
        paddingRight: "30px",
        paddingLeft: "30px"
    },
    buttonContainer: {
        display: "flex",
        justifyContent: "center",
        padding: "3%"
    },
    button: {
        color: "#efeaea",
        backgroundColor: "darkblue",
        borderRadius: "30px",
        margin: "30px"
    }

}))

export interface IPAdmin {
    email: string
    firstName: string
    lastName: string

}


const MyProfileInfo: FC = () => {
    const classes = useStyles();
    const [profileInfoShow, setprofileInfoShow] = useState(true)
    const [editProfileInfoShow, seteditProfileInfoShow] = useState(false)
    // const [admin, setAdmin] = useState<IPAdmin>()
     const adminApi = useContext(AdminApiContext);
 /*    const [email, setEmail] = useState<string>('');
     const [firstName, setFirstName] = useState<string>('');
     const [lastName, setLastName] = useState<string>('');*/
     const [admin, setAdmin] = useState<IPAdmin>();


    const onClicked = () => {
        seteditProfileInfoShow(true)
        setprofileInfoShow(false)
    }


     useEffect(() => {
         const fetchData =  () => {

              adminApi.fetchGetAdminById().then(admins => {
                 setAdmin(admins)
               /*  setFirstName(admins)
                 setLastName(admins)
                 console.log(admins)*/
                 /* setB(admins)
                  setC(admins)*/
             })
            /* setEmail(props.email)
             setFirstName(props.firstName)
             setLastName(props.lastName)*/
         };
         const timer = setTimeout(() => {
             fetchData();
         }, 1000);
         return () => clearTimeout(timer);
     }, []);

const profileInfo = () => {


    return (

                <Paper className={classes.root}>
                    <h1 className={classes.h1}>Mina uppgifter</h1>
                    <div className={classes.row}>

                        <div className={classes.column}>
                            <p>Personnummer:</p>
                            <p>1995xxxxxxxx</p>
                        </div>
                        <div className={classes.column}>
                            <p>KundNummer:</p>
                            <p>134xxxxxxxx</p>
                        </div>
                    </div>
                    <div>
                        <h4>Kontaktuppgifter</h4>
                    </div>
                    <div>

                        <div className={classes.row}>

                            <div className={classes.column}>
                                <p>Förnamn:</p>
                                <p>{admin?.firstName}</p>
                            </div>
                            <div className={classes.column}>
                                <p>Efternamn:</p>
                                <p>{admin?.lastName}</p>
                            </div>
                        </div>

                        <div className={classes.row}>

                            <div className={classes.column}>
                                <p>E-mail:</p>
                                <p>{admin?.email}</p>

                            </div>
                            <div className={classes.column}>
                                <p>Telefonnummer:</p>
                                <p>079999999</p>
                            </div>
                        </div>

                        <div className={classes.row}>

                            <div className={classes.column}>
                                <p>c/o</p>
                                <p>me</p>
                            </div>

                            <div>
                                <p>HemAdress</p>
                                <p>vägen32</p>
                            </div>
                        </div>

                        <div className={classes.row}>

                            <div className={classes.column}>

                                <p>PostOrt</p>
                                <p>stockholm</p>
                            </div>
                            <div className={classes.column}>
                                <p>Land</p>
                                <p>Sverige</p>

                            </div>
                        </div>
                        <div className={classes.buttonContainer}>
                            <Button className={classes.button}
                                    onClick={onClicked}
                                    variant={"contained"}
                            >Uppdatera mina uppgifter
                            </Button>
                        </div>
                    </div>
                </Paper>


   )
}

return (
   <div className={classes.root}>
       {
           profileInfoShow ? profileInfo() : null
       }
       {
           editProfileInfoShow ? <EditMyProfile/> : null
       }

   </div>

);
}
export default MyProfileInfo;
