import React, {FC, useState} from "react";
import {Button, Paper} from "@mui/material";
import {makeStyles} from "@material-ui/core";
import EditMyProfile from "./myPagesInfo/EditMyProfile";
import SchoolRequestList from "../listComponents/schoolRequest/SchoolRequestList";

const useStyles = makeStyles(() => ({
    container: {
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        flexDirection: "column",
        width: "500px",
        padding: 0,
        margin: 0,

        "@media (max-width: 650px)": {
            width: "300px",
        }
    },
    paperContainer: {
        display: "flex",
        flexDirection: "column",
        height: '100%',
        width: '50%',
        alignItems: "center",
        justifyContent: "center",
        margin: "20px"
    },
    heading: {
        display: "flex",
        justifyContent: "center",
        alignItems: "center",

        "@media (max-width: 650px)": {
            fontSize: "15px"
        }
    },
    requestButtonContainer: {
        display: "flex",
        justifyContent: "center",
        padding: "10%",

        "@media (max-width: 650px)": {
            width: "200px",
            fontSize: "10px"
        }
    },

}));
const SchoolRequestsComponent: FC = () => {
    const classes = useStyles();
    const [adminSchoolButton, setadminSchoolButton] = useState(true)
    const [showSchoolRequest, setshowSchoolRequest] = useState(false)
    const onClicked = () => {
        setadminSchoolButton(false)
        setshowSchoolRequest(true)
    }

    const schoolRequestButton = () => {
        return (
            <div className={classes.container}><Paper className={classes.paperContainer} sx={{borderRadius: "20px"}}>
                <h3
                    className={classes.heading}>
                    Skol förfrågningar
                </h3>
                <div className={classes.requestButtonContainer}>
                    <Button
                        onClick={onClicked}
                        sx={{
                            borderRadius: "30px",
                            backgroundColor: "darkblue",
                            color: "white",
                            fontSize: "12px"
                        }} variant="contained">Administrera
                    </Button>
                </div>
            </Paper></div>
        );
    }
    return (
        <div>
            {adminSchoolButton ? schoolRequestButton() : null}
            {showSchoolRequest ? <SchoolRequestList/> : null}
        </div>

    )
};

export default SchoolRequestsComponent;