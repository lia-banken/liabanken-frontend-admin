import React, {FC, useState} from 'react';
import {MenuItem, MenuList, Paper} from "@mui/material";
import {Grid, makeStyles} from "@material-ui/core";
import {useHistory} from "react-router-dom";
import Button from "@mui/material/Button";


const useStyles = makeStyles(() => ({
    menuContainer: {
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        flexDirection: "column",
        width: "100%",
        padding: 0,
        margin: 0,
        border: "1px solid",
    },
    paperContainer: {
        height: '100%',
        width: '100%',
        alignItems: "center",
        justifyContent: "center"

    },
    menuList: {
        display: "flex",
        height: "auto",
        width: "100",
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center",
        border: "1px solid"

    },
    heading: {
        display: "flex",
        color: "white",
        width: "100%",
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: "darkblue",
    },
    button: {
        display: "flex",
        justifyContent: "left",
        width: "100%",
        textTransform: "none"
    },
    buttonHideMenu: {
        padding: "10px",
        display: "flex",
        justifyContent: "center",
        width: "100%",
        textTransform: "none",
        color: "#efeaea",
        backgroundColor: "#081992",
        boxShadow: "unset",
        border: "none",
        margin: "0",
        "&:hover": {
            boxShadow: "none",
            backgroundColor: "#4c5499",
        },
        "&:active": {
            boxShadow: "none",
            backgroundColor: "#243592",
        }
    },
}));

const Menu: FC = () => {
    const history = useHistory()
    const [showAndHideMenu, setShowAndHideMenu] = useState<boolean>(true);
    const navigateAndHideMenu = (string: string) => {
        history.push(string);
        handleShowAndHideMenu();
    }
    const handleShowAndHideMenu = () => {
        setShowAndHideMenu(!showAndHideMenu);
    }

    const classes = useStyles();
    return (
        <div className={classes.menuContainer}>
            <Paper className={classes.paperContainer}>
                <MenuList className={classes.menuList}>
                    <h3 className={classes.heading}>Administratörens för- och efternamn</h3>
                    {showAndHideMenu && <div className={classes.menuList}>
                        <Button className={classes.button}
                                onClick={() => navigateAndHideMenu("/admin/minaSidor")}>Hem</Button>
                        <Button className={classes.button}
                                onClick={() => navigateAndHideMenu("/admin/minaSidor/MyProfileInfo")}>Mina
                            uppgifter</Button>
                        <Button className={classes.button}
                                onClick={() => navigateAndHideMenu("/admin/minaSidor/skolor")}>Mina
                            Skolor</Button>
                        <Button className={classes.button}
                                onClick={() => navigateAndHideMenu("/admin/minaSidor/MyStudnets")}>Mina
                            Elever</Button>
                        <Button className={classes.button}
                                onClick={() => navigateAndHideMenu("/admin/minaSidor/MyCompoany")}>Mina
                            Företag</Button>
                        <Button className={classes.button}
                                onClick={() => navigateAndHideMenu("/admin/minaSidor/Settings")}>Mina
                            inställningar</Button>
                        <Button className={classes.button}
                                onClick={() => navigateAndHideMenu("/admin/minaSidor/Administration")}>Alla
                            Administratörer</Button>
                    </div>}
                    <button className={classes.buttonHideMenu}
                            onClick={() => handleShowAndHideMenu()}>Mina sidor meny
                    </button>

                </MenuList>
            </Paper>
        </div>

    );
}
export default Menu;