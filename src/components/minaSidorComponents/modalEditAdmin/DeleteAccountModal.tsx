import * as React from 'react';
import Box from '@mui/material/Box';
import Modal from '@mui/material/Modal';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import {Link} from "@mui/material";
import {makeStyles} from "@material-ui/core";

const style = {
    position: 'absolute' as 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};
const useStyles = makeStyles((theme) => ({
    myPagesForm: {
        display: "flex",
        justifyContent: "center",
        flexDirection: "column",
        alignItems: "center"
    },
    textField:{
        marginBottom:"15px"
    },
    buttonSection:{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        marginTop:"20px",

    },
    button:{
        '&:hover':{color:"red"},

    },

    deleteLink:{
        '&:hover':{ color:"red",
            cursor:"pointer"}
    }

}));

export default function DeleteAccountModal() {
    const [open, setOpen] = React.useState(false);
    const handleOpen = () => setOpen(true);
    const handleClose = () => setOpen(false);
    const classes = useStyles();
    return (
        <div>
            <Link onClick={handleOpen} className={classes.deleteLink}>Ta bort konto</Link>
            <Modal
                keepMounted
                open={open}
                onClose={handleClose}
            >
                <Box sx={style}>
                    <Typography >
                        <h3>Ta bort konto</h3>

                    </Typography>
                    <Typography id="keep-mounted-modal-description" sx={{ mt: 2 }}>
                        Är du säker att du vill ta bort konto
                    </Typography>
                    <div className={classes.buttonSection} >
                        <Button className={classes.button} variant="outlined" >Ja</Button>
                        <Button className={classes.button} onClick={handleClose} variant="outlined" >Close</Button>
                    </div>
                </Box>
            </Modal>
        </div>
    );
}