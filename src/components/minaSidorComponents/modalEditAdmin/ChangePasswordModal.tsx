import * as React from 'react';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Modal from '@mui/material/Modal';
import {makeStyles} from "@material-ui/core";
import {Link, TextField} from "@mui/material";

const style = {
    position: 'absolute' as 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};
const useStyles = makeStyles((theme) => ({
    myPagesForm: {
        display: "flex",
        justifyContent: "center",
        flexDirection: "column",
        alignItems: "center"
    },
    textField:{
        marginBottom:"15px"
    },
    button:{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        marginTop:"20px"
    },
    changeLink:{
        '&:hover':{ color:"blue",
            cursor:"pointer"
        }
    },

}));

export default function ChangePasswordModal() {
    const classes = useStyles();
    const [open, setOpen] = React.useState(false);
    const handleOpen = () => setOpen(true);
    const handleClose = () => setOpen(false);


    return (
        <div>
            {/*<Button onClick={handleOpen}>Byta lösenord</Button>*/}
            <Link onClick={handleOpen}  className={classes.changeLink}>Byta lösenord</Link>
            <Modal
                open={open}
                onClose={handleClose}

            >
                <Box sx={style}>
                    <Typography >
                        Ändra lösenord
                    </Typography>
                    <Typography id="modal-modal-description" sx={{ mt: 2 }}>
                        <div>
                            <form className={classes.myPagesForm}>
                                <div className={classes.textField }>
                                    <TextField  label="Gammalt lösenord"/>
                                </div>
                                <div className={classes.textField}>
                                    <TextField  label="Nytt lösenord"/>
                                </div>
                                <div className={classes.textField}>
                                <TextField  label="Bekkräfta lösenord"/>
                                </div>
                            </form>
                            <div className={classes.button} >
                                <Button variant="outlined" >Spara</Button>
                                <Button onClick={handleClose} variant="outlined" >Close</Button>
                            </div>
                        </div>
                    </Typography>
                </Box>
            </Modal>
        </div>
    );
}