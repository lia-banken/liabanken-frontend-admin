import * as React from 'react';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Modal from '@mui/material/Modal';
import {makeStyles} from "@material-ui/core";
import {Link, TextField} from "@mui/material";
import {FormEvent, useState} from "react";

/*const url = {url:  "http://localhost:3000/admin/MyPagesHome/Administration"}*/

const style = {
    position: 'absolute' as 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};
const useStyles = makeStyles((theme) => ({
    myPagesForm: {
        display: "flex",
        justifyContent: "center",
        flexDirection: "column",
        alignItems: "center"
    },
    textField: {
        marginBottom: "15px"
    },
    button: {
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        marginTop: "20px",
    },
    adminButton: {
        '&:hover': {
            color: "blue",
            cursor: "pointer"
        }
    },
    cancleButton: {
        '&:hover': {
            color: "red",
            cursor: "pointer"
        }
    }
}));

export interface IListAdmin {
    /*   email: string,
       password: string,
       firstName: string
       lastName:string*/
    onAdd(firstName: string, lastName: string, email: string, userName: string, password: string,): void

}

const CreateAdminModal: React.FC<IListAdmin> = ({onAdd}) => {
    const classes = useStyles();
    const [open, setOpen] = React.useState(false);
    const [email, setEmail] = useState<string>('');
    const [password, setPassword] = useState<string>('');
    const [firstName, setFirstName] = useState<string>('');
    const [lastName, setLastName] = useState<string>('');
    const [userName, setUserName] = useState<string>('');
    const handleOpen = () => setOpen(true);
    const handleClose = () => setOpen(false);


    const addAdmin = (e: FormEvent<HTMLFormElement>): void => {
        e.preventDefault()
        onAdd(firstName, lastName, email, userName, password)
        setEmail('')
        setPassword('')
        setFirstName('')
        setLastName('')
        setUserName('')
        //  handleClose()
        window.location.reload();


        //console.log(onAdd)
    }


    return (
        <div>
            {/*<Button onClick={handleOpen}>Byta lösenord</Button>*/}
            <Button onClick={handleOpen} className={classes.adminButton}>Skapa Admin</Button>
            <Modal
                open={open}
                onClose={handleClose}
            >
                <Box sx={style}>
                    <Typography>
                        Skapa Admin
                    </Typography>
                    <Typography id="modal-modal-description" sx={{mt: 2}}>
                        <div>
                            <form className={classes.myPagesForm} onSubmit={addAdmin}>
                                <div className={classes.textField}>
                                    <TextField label="firstname"
                                               required
                                               onChange={(e) => setFirstName(e.target.value)}
                                    />
                                </div>
                                <div className={classes.textField}>
                                    <TextField label="lastname"
                                               required
                                               onChange={(e) => setLastName(e.target.value)}
                                    />
                                </div>
                                <div className={classes.textField}>
                                    <TextField label="email"
                                               required
                                               onChange={(e) => setEmail(e.target.value)}
                                    />
                                </div>
                                <div className={classes.textField}>
                                    <TextField label="userName"
                                               required
                                               onChange={(e) => setUserName(e.target.value)}
                                    />
                                </div>
                                <div className={classes.textField}>
                                    <TextField label="password"
                                               required
                                               onChange={(e) => setPassword(e.target.value)}
                                    />
                                </div>

                                <div className={classes.button}>
                                    <Button variant="outlined" type='submit'>Skapa</Button>
                                    <Button className={classes.cancleButton} onClick={handleClose}
                                            variant="outlined">Close</Button>
                                </div>
                            </form>
                        </div>
                    </Typography>
                </Box>
            </Modal>
        </div>
    );
}
export default CreateAdminModal;
