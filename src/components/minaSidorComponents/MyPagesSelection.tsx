import * as React from 'react';
import Box from '@mui/material/Box';
import Tab from '@mui/material/Tab';
import TabContext from '@mui/lab/TabContext';
import TabList from '@mui/lab/TabList';
import TabPanel from '@mui/lab/TabPanel';
import {useHistory} from "react-router-dom";
import {makeStyles} from "@material-ui/styles";

import Settings from "./Settings";
import {useContext, useEffect, useState} from "react";
import SchoolList from "../listComponents/schoolList/SchoolList";
import StudentList from "../listComponents/studentList/StudentList";
import CompanyList from "../listComponents/companyList/CompanyList";
import SchoolRequestsComponent from "./SchoolRequestsComponent";
import AdminstreraAdmin, {IPropsAdmin} from "../listComponents/administraterList/AdminstreraAdmin";
import MyProfileInfo, {IPAdmin} from "./myPagesInfo/MyProfileInfo";


const useStyles = makeStyles(() => ({
    root: {
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center",
        width: "100%"
    },
    p: {
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center",
        width: "100%",
        "@media (max-width: 650px)": {
            fontSize: "18px"
        }
    },
    alignTop: {
        display: "flex",
        verticalAlign: "top"
    },
    tabs: {
        width: "300px",
        border: "2px solid #d9d9d9"
    },
    profileInfo: {
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center"
    },
    underTabs: {
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center"
    },
    menu: {
        border: "none",
        "@media (max-width: 650px)": {
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
            justifyContent: "center",
            width: "70%",
            margin: "0 auto",
        }


    },
    menuList: {
        display: "flex",
        height: "auto",
        width: "100%",
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "left"
    },

    buttonHideMenu: {
        padding: "10px",
        display: "flex",
        justifyContent: "center",
        width: "100%",
        textTransform: "none",
        color: "#efeaea",
        backgroundColor: "#081992",
        boxShadow: "unset",
        border: "none",
        margin: "0",
        "&:hover": {
            boxShadow: "none",
            backgroundColor: "#4c5499",
        },
        "&:active": {
            boxShadow: "none",
            backgroundColor: "#243592",
        },

        "@media (max-width: 650px)": {
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
            justifyContent: "center",
            width: "300px",
        },
        "@media (min-width: 650px)": {
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
            justifyContent: "center",
            width: "300px",
            margin: "0 auto",
        }
    },
}));

const MyPagesSelection: React.FC = () => {
    const classes = useStyles();
    const history = useHistory()
    const [admin, setAdmin] = useState<IPropsAdmin[]>([]);
    const [value, setValue] = React.useState('1');
    const [showAndHideMenu, setShowAndHideMenu] = useState<boolean>(true);


    const navigateAndHideMenu = (string: string) => {
        history.push(string);
        handleShowAndHideMenu();
    }
    const handleShowAndHideMenu = () => {
        setShowAndHideMenu(!showAndHideMenu);
    }

    const handleChange = (event: React.SyntheticEvent, newValue: string) => {
        setValue(newValue);
    }

    return (
        <div className={classes.root}>
            <Box style={{overflow: 'auto'}}>
                <h2 className={classes.p}>Välkommen till mina sidor</h2>
                <TabContext value={value}>
                    <div>
                        <Box>
                            <div className={classes.menu}>
                                <button className={classes.buttonHideMenu}
                                        onClick={() => handleShowAndHideMenu()}>Mina sidor meny
                                </button>
                                {showAndHideMenu && <div className={classes.menuList}>
                                    <TabList
                                        className={classes.tabs} orientation="vertical" onChange={handleChange}>
                                        <Tab onClick={() => navigateAndHideMenu("/admin/MyPagesHome")} label="Hem"
                                             value="1"/>
                                        <Tab onClick={() => navigateAndHideMenu("/admin/MyPagesHome/MinaUppgifter")}
                                             label="Mina Uppgifter" value="2"/>
                                        <Tab onClick={() => navigateAndHideMenu("/admin/MyPagesHome/skolor")}
                                             label="Mina skolor" value="3"/>
                                        <Tab onClick={() => navigateAndHideMenu("/admin/MyPagesHome/MyStudnets")}
                                             label="Mina Elever" value="4"/>
                                        <Tab onClick={() => navigateAndHideMenu("/admin/MyPagesHome/MyCompoany")}
                                             label="Mina Företag" value="5"/>
                                        <Tab onClick={() => navigateAndHideMenu("/admin/MyPagesHome/Settings")}
                                             label="inställningar" value="6"/>
                                        <Tab onClick={() => navigateAndHideMenu("/admin/MyPagesHome/Administration")}
                                             label="administration" value="7"/>
                                    </TabList>
                                </div>}
                            </div>
                        </Box>


                        <div>
                            <TabPanel className={classes.underTabs} value="1">
                                <SchoolRequestsComponent/>
                            </TabPanel>

                            <TabPanel className={classes.profileInfo} value="2">
                                <MyProfileInfo/>{/*email={email} firstName={firstName} lastName={lastName}*/}
                            </TabPanel>

                            <TabPanel value="3"> <SchoolList/></TabPanel>
                            <TabPanel value="4"> <StudentList/></TabPanel>
                            <TabPanel value="5"><CompanyList/></TabPanel>
                            <TabPanel className={classes.profileInfo} value="6">
                                <Settings/>
                            </TabPanel>
                            <TabPanel value="7">
                                <AdminstreraAdmin list={admin}/>
                            </TabPanel>
                        </div>
                    </div>

                </TabContext>

            </Box>

        </div>

    );
}
export default MyPagesSelection;
