import React from 'react';
import {makeStyles} from "@material-ui/core";

const useStyles = makeStyles(() => ({
    articleContainer: {
        border: "1px solid #081992",
        margin: "2%",
        marginTop: "3%",
        marginBottom: "3%",
        padding: "5%"
    },
    titleText: {
        color: "#081992",
        fontWeight: "bolder",
        padding: 0,
        margin: 0
    },
    text: {
        padding: 0,
        margin: 0
    }
}));

interface IArticle {
    titleText: string;
    text: string;
}

const Article: React.FC<IArticle> = ({titleText, text}) => {
    const classes = useStyles();
    return (
        <div className={classes.articleContainer}>
            <p className={classes.titleText}>{titleText}</p>
            <p className={classes.text}>{text}</p>
        </div>
    );
};

export default Article;
