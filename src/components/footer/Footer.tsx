import React from 'react';
import {makeStyles} from "@material-ui/core";
import LanguageIcon from '@mui/icons-material/Language';
import LocalPhoneIcon from '@mui/icons-material/LocalPhone';
import StarIcon from '@mui/icons-material/Star';
import FooterItem from "./FooterItem";
import {Link} from "@mui/material";

const useStyles = makeStyles(() => ({
    footer: {
        bottom: 0, //Here is the trick
        backgroundColor: "#081992",
        color: "#efeaea",
    },
    footerElement: {
        display: "flex",
        padding: "3%"
    },
    footerBottom: {
        display: "flex",
        alignItems: "center",
        justifyContent: "space-between"
    },
    links: {
        display: "flex",
        flexDirection: "column",
        marginRight: "20px",
        "@media (min-width: 1000px)": {
            flexDirection: "row",
            alignItems: "center",

        },
    },
}));

const Footer: React.FC = () => {
    const classes = useStyles();
    return (
        <div className={classes.footer}>
            <div>
                <div className={classes.footerElement}>
                    <LanguageIcon/>
                    <FooterItem text={"Other Languages"}/>
                </div>
                <div className={classes.footerElement}>
                    <LocalPhoneIcon/>
                    <FooterItem text={"Kontakta oss"}/>
                </div>

                <div className={classes.footerBottom}>

                    <div data-testid="startIcon" className={classes.footerElement}>
                        <div style={{display: "flex", flexDirection: "column"}}>
                            <div>
                                <StarIcon/>
                                <FooterItem text={"Liabanken "}/>
                            </div>
                            <div>
                                <StarIcon/>
                                <FooterItem text={" Swedish public "}/>
                            </div>
                        </div>
                    </div>

                    <div className={classes.links}>
                        <p>
                            Följ oss på:
                        </p>
                        <Link style={{marginBottom: "5px", marginTop: "5px"}} href="#" underline="none">
                            Facebook
                        </Link>
                        <Link style={{marginBottom: "5px", marginTop: "5px"}} href="#" underline="none">
                            LinkedIn
                        </Link>
                        <Link style={{marginBottom: "5px", marginTop: "5px"}} href="#" underline="none">
                            Youtube
                        </Link>
                    </div>

                </div>
            </div>
        </div>
    );
};

export default Footer;
