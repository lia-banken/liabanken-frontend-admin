import React from 'react';
import { render, screen } from '@testing-library/react';
import FooterItem from "../FooterItem";


test('test render FooterItem correct', () => {
    render(<FooterItem text="Arne"/>);
    const linkElement = screen.getByTestId("view-text-footer-item");
    expect(linkElement).toBeInTheDocument();
    expect(linkElement).toBeVisible();
    expect(linkElement).toMatchSnapshot();
    expect(linkElement).toHaveAttribute("value","Arne");
});
