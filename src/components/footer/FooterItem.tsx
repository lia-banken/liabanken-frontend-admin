import React from 'react';
import {Button, SvgIconTypeMap} from "@mui/material";
import {OverridableComponent} from "@mui/material/OverridableComponent";
import {makeStyles} from "@material-ui/core";


const useStyles = makeStyles(() => ({
    footerItem: {
        marginLeft: "2%"
    }
}));

interface IFooterItem {
    text: string;

}

const FooterItem: React.FC<IFooterItem> = ({text}) => {
    const classes = useStyles();
    return (
        <Button variant={"text"}
                data-testid="view-text-footer-item"
                className={classes.footerItem}
                color="info"
        >
            {text}
        </Button>
    );
};

export default FooterItem;
