/*import * as React from "react";
import {makeStyles} from "@material-ui/styles";*/
import {FC, useState} from "react";
import {Box, TextField} from "@mui/material";
import {Button, Divider} from "@material-ui/core";
import MyProfileInfo from "../minaSidorComponents/myPagesInfo/MyProfileInfo";
import SchoolRequestList from "../listComponents/schoolRequest/SchoolRequestList";
import SchoolList from "../listComponents/schoolList/SchoolList";
import SchoolProfile from "./SchoolProfile";
import ArrowBackIosIcon from '@mui/icons-material/ArrowBackIos';



/*
const useStyles = makeStyles(() => ({
    root: {
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center",
    },
    oneRow: {
        margin: "20px",

    },
    header: {
        display: "flex",
        justifyContent: "center"
    },
    buttonContainer: {
        display: "flex",
        justifyContent: "center",
        padding: "3%"
    },
    buttonBack:{
        color: "#efeaea",
        backgroundColor: "#081992",
        borderRadius: "30px",
        margin: "30px"
    },
    button: {
        color: "#efeaea",
        backgroundColor: "#081992",
        borderRadius: "30px",
        margin: "30px"
    }
}));
*/

//const EditSchoolProfile: FC = () => {
/*    const classes = useStyles();
    const [schoolProfileShow, setSchoolProfileShow] = useState(false)
    const [editSchoolProfileShow, setEditSchoolProfileShow] = useState(true)
    const [schoolListShow, setSchoolListShow] = useState(false)
    const onClicked = () => {
        setSchoolProfileShow(true)
        setEditSchoolProfileShow(false)
    }
    const onClickedBack = () => {
        setSchoolProfileShow(false)
        setEditSchoolProfileShow(false)
        setSchoolListShow(true)
    }
        const editSchoolProfileInfo = () => {
            return (
                <Box
                    component="form"
                    sx={{'& .MuiTextField-root': {m: 1, width: '25ch'},}}
                    noValidate
                    autoComplete="off"
                >

                    <Button className={classes.buttonBack}
                            variant="contained"
                            startIcon={<ArrowBackIosIcon />}
                            onClick={onClickedBack}
                    >
                        Tillbaka
                    </Button>
                    <h2>Redigera uppgifter till "Skolans namn här"</h2>
                    <div className={classes.root}>
                        <div className={classes.oneRow}>
                            <TextField id="outlined-basic" label="Skolans namn" variant="outlined"/>
                            <TextField id="outlined-basic" label="Skolans telefonnummer" variant="outlined"/>
                        </div>

                        <div className={classes.oneRow}>
                            <TextField id="outlined-basic" label="Skolans E-mail" variant="outlined"/>
                            <TextField id="outlined-basic" label="Websida" variant="outlined"/>
                        </div>

                        <div className={classes.oneRow}>
                            <TextField id="outlined-basic" label="Besöks adress" variant="outlined"/>
                            <TextField id="outlined-basic" label="Postnummer" variant="outlined"/>
                        </div>

                        <div className={classes.oneRow}>
                            <TextField id="outlined-basic" label="Postort" variant="outlined"/>
                            <TextField id="outlined-basic" label="Land" variant="outlined"/>
                        </div>
                        <Divider/>
                        <div>
                            <h4 className={classes.header}>Kontakt person</h4>
                        </div>
                        <div className={classes.oneRow}>
                            <TextField id="outlined-basic" label="FörNamn" variant="outlined"/>
                            <TextField id="outlined-basic" label="EfterNamn" variant="outlined"/>
                        </div>
                        <div className={classes.oneRow}>
                            <TextField id="outlined-basic" label="E-mail" variant="outlined"/>
                            <TextField id="outlined-basic" label="Telefonnummer" variant="outlined"/>
                        </div>

                        <div className={classes.buttonContainer}>
                            <Button
                                className={classes.button}
                                onClick={onClicked}
                                variant={"contained"}
                            >Spara skolans uppgifter</Button>
                        </div>
                    </div>
                </Box>

            );
        }
    return (
        <div className={classes.root}>
            {
                schoolProfileShow ?<SchoolProfile id={''}/> : null
            }
            {
                editSchoolProfileShow ?  editSchoolProfileInfo(): null
            }
            {
                schoolListShow ?  <SchoolList/>: null
            }
        </div>
    );

}*/


//export default EditSchoolProfile;
