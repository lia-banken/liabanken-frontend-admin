import * as React from 'react';
import {FC, useContext, useEffect, useState} from "react";
import {Button, Card, Divider, makeStyles, Paper} from "@material-ui/core";

import SchoolListItem, {ISchoolItem} from "../listComponents/schoolList/SchoolListItem";
import ArrowBackIosIcon from "@mui/icons-material/ArrowBackIos";
import SchoolList from "../listComponents/schoolList/SchoolList";
import {AdminApiContext} from "../../App";



const useStyles = makeStyles(() => ({
    root: {
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center",
    },
    row: {
        display: "flex",
        flexDirection: "row",
        justifyContent: "space-between",
        margin: "30px"


    },
    column: {
        display: "flex",
        flexDirection: "column",
        justifyContent: "space-between",
        paddingRight: "30px",
        paddingLeft: "30px"
    },
    header: {
        display: "flex",
        justifyContent: "center"
    },
    buttonContainer: {
        display: "flex",
        justifyContent: "center",
        padding: "3%"
    },
    button: {
        color: "#efeaea",
        backgroundColor: "darkblue",
        borderRadius: "30px",
        margin: "30px"
    },
    buttonBack: {
        color: "#efeaea",
        backgroundColor: "#081992",
        borderRadius: "30px",
        margin: "30px"
    },

}))

export interface params {
    id: string
    email:string
    schoolName:string
    organizationNumber:string
}
export interface P {
    id: string

}
const SchoolProfile: React.FC<P>= (props) => {
    const classes = useStyles();
    const [schoolprofileShow, setSchoolprofileShow] = useState(true)
   // const [editSchoolProfileShow, setEditSchoolProfileShow] = useState(false)
    const [schoolItem, setSchoolItem] = useState<ISchoolItem[]>([]);
    const [schoolInfo, setSchoolInfo] = useState<params>();
    const adminApi = useContext(AdminApiContext);
 /*   const onClicked = () => {
       // setEditSchoolProfileShow(true)
        setSchoolprofileShow(false)
    }*/
    const [schoolListShow, setSchoolListShow] = useState(false)

    const onClickedBack = () => {
        setSchoolprofileShow(false)
       // setEditSchoolProfileShow(false)
        setSchoolListShow(true)
    }


   /* useEffect(() => {
        const fetchData =  () => {
            adminApi.getApprovedSchoolsById().then(schools => setSchoolInfo(schools))
        };
        const timer = setTimeout(() => {
            fetchData();
        }, 1000);
        return () => clearTimeout(timer);
    }, []);*/
  /*  useEffect(()=>{
        console.log("receved id profile: "+props.id)
        const getSchoolById = async (id: string) => {
        await fetch("http://localhost:8080/school/get-school/" + props.id, {method: "GET"})
            .then(response => {
                return response.json()
                    .then(school => {
                        console.log(school);
                        setSchoolItem(school)
                       })
            });
    };})*/

    const schoolprofileInfo = () => {
        return (
            <div>
              {/*  {schoolItem.map((item )=>(

                    <div className={classes.row}>
                        <div className={classes.column}>
                            <p>Organisationsnummer:</p>
                            <p>{item.organizationNumber}</p>
                        </div>
                        <div className={classes.column}>
                            <p>KundNummer:</p>
                            <p>{item.id}</p>
                        </div>
                    </div>
                ))}*/}
                    <Button className={classes.buttonBack}
                              variant="contained"
                              startIcon={<ArrowBackIosIcon/>}
                              onClick={onClickedBack}
                    >
                        Tillbaka</Button>


                <Paper className={classes.root}>
                    <h1> Skol uppgifter</h1>



                        <Divider/>

                        <div>
                            <h4 className={classes.header}>Kontaktuppgifter</h4>
                        </div>
                        <div>
                            <div className={classes.row}>

                                <div className={classes.column}>
                                    <p>Namn:{schoolInfo?.email}</p>
                                    <p>SkolaAB{schoolInfo?.schoolName}</p>
                                </div>
                                <div className={classes.column}>
                                    <p>E-mail:</p>
                                    <p>{schoolInfo?.organizationNumber}</p>

                                </div>
                            </div>

                            <div className={classes.row}>
                                <div className={classes.column}>
                                    <p>Telefonnummer:</p>
                                    <p>079999999</p>
                                </div>
                                <div className={classes.column}>
                                    <p>websida:</p>
                                    <p>www.skolaAB.se</p>

                                </div>
                            </div>

                            <div className={classes.row}>

                                <div className={classes.column}>
                                    <p>BesöksAdress</p>
                                    <p>skol gatan 3</p>
                                </div>

                                <div>
                                    <p>Postnummer</p>
                                    <p>123 45</p>
                                </div>
                            </div>

                            <div className={classes.row}>

                                <div className={classes.column}>

                                    <p>PostOrt</p>
                                    <p>stockholm</p>
                                </div>
                                <div className={classes.column}>
                                    <p>Land</p>
                                    <p>Sverige</p>
                                </div>
                            </div>
                            <Divider/>
                            <div>
                                <div>
                                    <h4 className={classes.header}>Kontakt person</h4>
                                </div>
                                <div className={classes.row}>
                                    <div className={classes.column}>
                                        <p>Förnamn:</p>
                                        <p>Goerge</p>
                                    </div>
                                    <div className={classes.column}>
                                        <p>Efternamn</p>
                                        <p>Lucas</p>

                                    </div>
                                </div>
                                <div className={classes.row}>
                                    <div className={classes.column}>
                                        <p>TelefonNummer:</p>
                                        <p>070 123 4567</p>
                                    </div>
                                    <div className={classes.column}>
                                        <p>E-mail:</p>
                                        <p>goerge.lucas@gmail.com</p>
                                    </div>
                                </div>
                            </div>

                            <div className={classes.buttonContainer}>
                              {/*  <Button className={classes.button}
                                        onClick={onClicked}
                                        variant={"contained"}
                                >Uppdatera skolans uppgifter
                                </Button>*/}
                            </div>
                        </div>
                    </Paper>

            </div>
        )
    }

    return (
        <div className={classes.root}>
            {
                schoolprofileShow ? schoolprofileInfo() : null
            }
       {/*     {
                editSchoolProfileShow ? <EditSchoolProfile/> : null
            }*/}
            {
                schoolListShow ? <SchoolList/> : null
            }


        </div>

    );
}


export default SchoolProfile;
