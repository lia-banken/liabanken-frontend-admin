import * as React from 'react';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemText from '@mui/material/ListItemText';
import ListItemAvatar from '@mui/material/ListItemAvatar';

import Avatar from '@mui/material/Avatar';
import {makeStyles} from "@material-ui/core";
import {Divider} from "@mui/material";
import DeleteAccountModal from "../../minaSidorComponents/modalEditAdmin/DeleteAccountModal";
import DeleteAdminModal from "../../minaSidorComponents/modalEditAdmin/DeleteAdminModal";
import CreateAdminModal from "../../minaSidorComponents/modalEditAdmin/CreateAdminModal";
import {useContext, useEffect, useState} from "react";
import API from "../../../route/API";
import AdminListItem, {IAdminItem} from "./AdminListItem";
import {AdminApiContext} from "../../../App";

const useStyles = makeStyles(() => ({
    header: {
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        flexDirection: "row",
        margin: "30px",
        "@media (max-width: 650px)": {
            fontSize: "13px",

        }

    },
    listItem: {
        marginTop: "30px"
    },
    item: {
        marginBottom: "50px"
    },
    button: {
        marginLeft: "50px",
        color: "white",
        '&:hover': {
            color: "blue",
            cursor: "pointer",
            border: "0.1px solid gray"
        }

    },


}));

export interface IPropsAdmin {
    firstName: string
    lastName: string
    email: string
    userName: string
    password: string
}

export interface IPropsAdminList {
    list: IPropsAdmin[]
}

const AdminstreraAdmin: React.FC<IPropsAdminList> = (props) => {
    const classes = useStyles();
    const [admin, setAdmin] = useState<IPropsAdmin[]>([]);
    const [adminList, setAdminList] = useState<IAdminItem[]>([])
    const adminApi = useContext(AdminApiContext);

    const addAdminn = async (firstName: string, lastName: string, email: string, userName: string, password: string,) => {
        await fetch("http://localhost:8080/admin/register_new_admin_user?firstName=" + firstName + "&lastName=" + lastName + "&email=" + email + "&userName=" + userName + "&password=" + password, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            },
        })
    }

    useEffect(() => {
        adminApi.fetchAllAdmins().then(admins => setAdminList(admins))
    }, []);

    return (
        <div>
            <div className={classes.header}>
                <h2>Alla administratörer</h2>
                {/*<button className={classes.button1}>Skapa Admin</button>*/}
                <div className={classes.button}>
                    <CreateAdminModal onAdd={addAdminn}/>
                </div>
            </div>
            <Divider/>
            <div>
                <AdminListItem list={adminList}/>
            </div>
        </div>
    );
}
export default AdminstreraAdmin;

