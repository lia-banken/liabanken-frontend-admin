import React from 'react';
import {Button, makeStyles} from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
    title: {
        color: "#081992",
    },
    adminItem: {
        border: "1px solid lightgrey",
    },
    buttonContainer: {
        margin: "10px"
    }
}))

export interface IAdminItem {
    email: string,
    password: string
    firstName: string
    lastName: string
}

interface IAdminList {
    list: IAdminItem[]
}

const AdminListItem: React.FC<IAdminList> = ({list}) => {
    const classes = useStyles();
    return (
        <div>
            {list.map((item) => (
                <div className={classes.adminItem}>
                    <h3 className={classes.title}>{item.firstName} {item.lastName}</h3>
                    <p>{item.email}</p>
                    <div className={classes.buttonContainer}>
                    </div>
                </div>
            ))}
        </div>
    );
};

export default AdminListItem;