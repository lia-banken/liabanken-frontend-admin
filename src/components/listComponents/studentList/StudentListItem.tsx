import React from 'react';
import {Button, makeStyles} from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
    title: {
        color: "#081992",
    },
    studentItem: {
        border: "1px solid lightgrey",
    },
    buttonContainer:{
        margin:"10px"
    }
}))
export interface IStudentItem{
    id:string
    firstName:string
    lastName:string
    addressDTO: { city: string }

}

interface IStudentList{
    list:IStudentItem[]
}

const StudentListItem: React.FC<IStudentList> = ({list}) => {
    const classes = useStyles();
    return (
        <div>
            {list.map((item) => (
                <div className={classes.studentItem}>
                    <h3 className={classes.title}>{item.firstName} {item.lastName}</h3>
                    <p>{item.addressDTO.city}</p>
                    <div className={classes.buttonContainer}>
                    </div>
                </div>


            ))}

        </div>
    );
};

export default StudentListItem;