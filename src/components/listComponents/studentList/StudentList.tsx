import React, {FC, useEffect, useState} from "react";
import {makeStyles} from "@material-ui/core";
import {Button, TextField} from "@mui/material";
import SearchIcon from '@mui/icons-material/Search';
import DropDownFilterCity from "../dropDownFilter/DropDownFilterCity";
import DropDownFilterBransch from "../dropDownFilter/DropDownFilterBransch";
import StudentListItem, {IStudentItem} from "./StudentListItem";
import API from "../../../route/API";

const useStyles = makeStyles(() => ({

    header: {
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        flexDirection: "row",
        margin: "30px",
        "@media (max-width: 650px)": {
            fontSize: "18px",

        }
    },
    input: {
        width: "80%",
        backgroundColor: 'white',
        border: 'none'
    },
    searchButton: {
        height: '55px',
        backgroundColor: "#081992",
        color: "#efeaea",
        "&:hover": {
            boxShadow: "none",
            backgroundColor: "#4c5499",
        },
        "@media (min-width: 800px)": {
            marginLeft: '0.5%'
        }
    },
    filterAction: {
        display: 'flex',
        margin: "20px",
        justifyContent: 'space-evenly',
        "@media (max-width: 500px)": {
            flexDirection: "column"
        }
    },
}));

const StudentList: FC = () => {
    const classes = useStyles();
    const [studentList, setStudentlist] = useState<IStudentItem[]>([]);
    /*    useEffect(() => {
            const fetchData = async () => {
                await API.fetchAllStudents().then(students => setStudentlist(students))

            };

            const timer = setTimeout(() => {
                fetchData();
            }, 1000);
            return () => clearTimeout(timer);
        }, []);

        const filterStudent = (e: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => {
            const tmpArray = studentList.filter(item => item.firstName.toLowerCase().includes(e.target.value))
            setStudentlist(tmpArray)
            if (e.target.value == "") {
                API.fetchAllStudents().then(students => setStudentlist(students))
            }
        };*/

    return (
        <div>
            <div>
                <h2 className={classes.header}>Mina Elever</h2>
                <div>
                    <TextField
                        className={classes.input}
                        id="search"
                        placeholder='Skriv in text...'
                        type="text"
                        // onChange={e => filterStudent(e)}
                    />
                    <Button
                        variant={'outlined'}
                        className={classes.searchButton}>
                        <SearchIcon/>
                        Sök
                    </Button>
                </div>
                <div className={classes.filterAction}>
                    <DropDownFilterCity/>
                    <DropDownFilterBransch/>
                </div>
            </div>
            <div>
                <StudentListItem list={studentList}/>
            </div>
        </div>
    );
}
export default StudentList;
