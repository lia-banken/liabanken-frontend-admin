import React, {FC, useContext, useEffect, useState} from "react";
import {makeStyles} from "@material-ui/core";
import {Button, TextField} from "@mui/material";
import SearchIcon from '@mui/icons-material/Search';
import DropDownFilterCity from "../dropDownFilter/DropDownFilterCity";
import DropDownFilterBransch from "../dropDownFilter/DropDownFilterBransch";
import SchoolListItem, {ISchoolItem} from "../schoolList/SchoolListItem";
import SchoolRequestListItem, {ISchooRequestItem} from "./SchoolRequestListItem";

import SchoolProfile from "../../schoolProfile/SchoolProfile";
import SchoolProfileAprove from "./SchoolProfileAprove";
import {log} from "util";
import API from "../../../route/API";
import {AdminApiContext} from "../../../App";


const useStyles = makeStyles(() => ({
    header: {
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        flexDirection: "row",
        margin: "30px",

        "@media (max-width: 650px)": {
            fontSize: "20px",
        }
    },
    input: {
        width: "80%",
        backgroundColor: 'white',
        border: 'none'
    },
    searchButton: {
        height: '55px',
        backgroundColor: "#081992",
        color: "#efeaea",
        "&:hover": {
            boxShadow: "none",
            backgroundColor: "#4c5499",
        },
        "@media (min-width: 800px)": {
            marginLeft: '0.5%'
        }
    },
    buttonContainer: {
        display: "flex",
        margin: "10px",
        flexDirection: "row",
        justifyContent: "space-between"
    }

}));


const SchoolRequestList: FC = () => {
    const classes = useStyles();
    const [schoolRequestList, setSchoolRequestList] = useState<ISchooRequestItem[]>([]);
    const [schoolProfileShow, setSchoolProfileShow] = useState(false)
    const [schoolRequestItemShow, setSchoolRequestItemShow] = useState(true)
    const [triggerRefersh, setTriggerRefersh] = useState()
    const adminApi = useContext(AdminApiContext);
    const toggleViews = () => {
        if (schoolRequestItemShow == true) {
            setSchoolRequestItemShow(false)
            setSchoolProfileShow(true)
        } else if (schoolProfileShow == true) {
            setSchoolRequestItemShow(false)
            setSchoolProfileShow(true)
        }
    };

    useEffect(() => {
        adminApi.fetchAllSchoolsRequest().then(schools => setSchoolRequestList(schools))
    }, []);

    const filterSchoolRequest = (e: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => {
        const tmpArray = schoolRequestList.filter(item => item.schoolName.toLowerCase().includes(e.target.value))
        setSchoolRequestList(tmpArray)
        if (e.target.value == "") {
            adminApi.fetchAllSchoolsRequest().then(schools => setSchoolRequestList(schools))
        }
    };

    const denyOnClick = async (id: string) => {
        await adminApi.deleteRequestSchool(id);
        adminApi.fetchAllSchoolsRequest().then(schools => setSchoolRequestList(schools));
    };
    const approveOnClick = async (id: string) => {
        await adminApi.approveSchool(id);
        adminApi.fetchAllSchoolsRequest().then(schools => setSchoolRequestList(schools));
    };


    const SchoolRequestListInfo = () => {
        return (
            <div>
                <div>
                    <h2 className={classes.header}>Skol Förfrågningar</h2>
                    <div>
                        <TextField
                            className={classes.input} id="search"
                            placeholder='Skriv in text...'
                            type="text"
                            onChange={e => filterSchoolRequest(e)}
                        />
                        <Button variant={'outlined'} className={classes.searchButton}>
                            <SearchIcon/>
                            Sök
                        </Button>
                    </div>
                </div>
                <div>
                    <SchoolRequestListItem list={schoolRequestList}
                                           onClick={toggleViews}
                                           denyButton={(id) => denyOnClick(id)}
                                           approveButton={(id) => approveOnClick(id)}
                    />
                </div>
            </div>
        );
    }

    return (
        <div>
            {schoolRequestItemShow && SchoolRequestListInfo()}
            {schoolProfileShow && <SchoolProfileAprove/>}
        </div>
    );
}
export default SchoolRequestList;
