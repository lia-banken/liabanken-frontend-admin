import * as React from 'react';
import {FC, useState} from "react";
import {Button, Divider, makeStyles, Paper} from "@material-ui/core";
import ArrowBackIosIcon from "@mui/icons-material/ArrowBackIos";
import SchoolRequestList from "./SchoolRequestList";


const useStyles = makeStyles(() => ({
    root: {
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center",
    },
    row: {
        display: "flex",
        flexDirection: "row",
        justifyContent: "space-between",
        margin: "30px"
    },
    column: {
        display: "flex",
        flexDirection: "column",
        justifyContent: "space-between",
        paddingRight: "30px",
        paddingLeft: "30px"
    },
    header: {
        display: "flex",
        justifyContent: "center"
    },
    buttonContainer: {
        display: "flex",
        justifyContent: "center",
        padding: "3%"
    },
    button: {
        color: "#efeaea",
        backgroundColor: "darkblue",
        borderRadius: "30px",
        margin: "30px"
    },
    buttonBack: {
        color: "#efeaea",
        backgroundColor: "#081992",
        borderRadius: "30px",
        margin: "30px"
    },

}))


const SchoolProfileAprove: FC = () => {
    const classes = useStyles();

    const [schoolprofileShow, setSchoolprofileShow] = useState(true)
    const [editSchoolProfileShow, setEditSchoolProfileShow] = useState(false)
    const onClicked = () => {
        setEditSchoolProfileShow(true)
        setSchoolprofileShow(false)
    }
    const [schoolRequestListShow, setSchoolRequestListShow] = useState(false)

    const onClickedBack = () => {
        setSchoolprofileShow(false)
        setSchoolRequestListShow(true)
    }


    const schoolprofileAproveInfo = () => {
        return (
            <div>
                <Button className={classes.buttonBack}
                        variant="contained"
                        startIcon={<ArrowBackIosIcon/>}
                        onClick={onClickedBack}
                >
                    Tillbaka</Button>
                <Paper className={classes.root}>

                    <h1>"SkolNamn" profil uppgifter</h1>
                    <div className={classes.row}>
                        <div className={classes.column}>
                            <p>Organisationsnummer:</p>
                            <p>1995xxxxxxxx</p>
                        </div>
                        <div className={classes.column}>
                            <p>Namn:</p>
                            <p>Skola AB</p>
                        </div>
                    </div>
                    <Divider/>
                    <div>
                        <h4 className={classes.header}>Kontaktuppgifter</h4>
                    </div>
                    <div>
                        <div className={classes.row}>
                            <div className={classes.column}>
                                <p>BesöksAdress</p>
                                <p>skol gatan 3</p>
                            </div>
                            <div className={classes.column}>
                                <p>E-mail:</p>
                                <p>goerge@gmail</p>
                            </div>
                        </div>
                        <div className={classes.row}>
                            <div className={classes.column}>
                                <p>Telefonnummer:</p>
                                <p>079999999</p>
                            </div>
                            <div className={classes.column}>
                                <p>websida:</p>
                                <p>www.skolaAB.se</p>
                            </div>
                        </div>
                        <div className={classes.row}>
                            <div className={classes.column}>
                                <p>BesöksAdress</p>
                                <p>skol gatan 3</p>
                            </div>
                            <div>
                                <p>Postnummer</p>
                                <p>123 45</p>
                            </div>
                        </div>
                        <div className={classes.row}>
                            <div className={classes.column}>
                                <p>PostOrt</p>
                                <p>stockholm</p>
                            </div>
                            <div className={classes.column}>
                                <p>Land</p>
                                <p>Sverige</p>
                            </div>
                        </div>
                        <Divider/>
                        <div>
                            <div>
                                <h4 className={classes.header}>Kontakt person</h4>
                            </div>
                            <div className={classes.row}>
                                <div className={classes.column}>
                                    <p>Förnamn:</p>
                                    <p>Goerge</p>
                                </div>
                                <div className={classes.column}>
                                    <p>Efternamn</p>
                                    <p>Lucas</p>
                                </div>
                            </div>
                            <div className={classes.row}>
                                <div className={classes.column}>
                                    <p>TelefonNummer:</p>
                                    <p>070 123 4567</p>
                                </div>
                                <div className={classes.column}>
                                    <p>E-mail:</p>
                                    <p>goerge.lucas@gmail.com</p>
                                </div>
                            </div>
                        </div>
                        <div className={classes.buttonContainer}>
                            <Button className={classes.button}
                                    onClick={onClicked}
                                    variant={"contained"}
                            >Uppdatera skolans uppgifter
                            </Button>
                        </div>
                    </div>
                </Paper>
            </div>
        )
    }
    return (
        <div className={classes.root}>
            {schoolprofileShow ? schoolprofileAproveInfo() : null}
            {schoolRequestListShow ? <SchoolRequestList/> : null}
        </div>
    );
}


export default SchoolProfileAprove;