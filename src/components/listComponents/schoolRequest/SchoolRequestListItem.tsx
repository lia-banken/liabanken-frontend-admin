import React from 'react';
import { Button, makeStyles} from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
    title: {
        color: "#081992",
    },
    schoolItem: {
        border: "1px solid lightgrey",
    },
    buttonContainer: {
        display: "flex",
        margin: "5px",
        flexDirection: "row",
        justifyContent: "space-between"
    }
}))

export interface ISchooRequestItem {
    id: string
    email: string
    schoolName: string
    organizationNumber: string
}

interface IOnClick {
    onClick: () => void
    denyButton: (id: string) => void
    approveButton:(id:string) => void
}

interface ISchooRequestList {
    list: ISchooRequestItem[]
}

const SchoolRequestListItem: React.FC<ISchooRequestList & IOnClick> = (props) => {
    const classes = useStyles();
    return (
        <div>
            {props.list.map((item) => (
                <div className={classes.schoolItem} key={item.id}>
                    <h3 className={classes.title}>{item.schoolName}</h3>
                    <p>{item.organizationNumber}</p>
                    <div className={classes.buttonContainer}>
                        <Button
                            onClick={() => props.approveButton(item.id)}
                            variant="contained"
                            color="primary">Godkänn</Button>
                        <Button
                            variant="contained"
                            color="primary"
                            onClick={() => props.denyButton(item.id)}
                        >
                            Neka
                        </Button>
                    </div>
                </div>
            ))}
        </div>
    );
};

export default SchoolRequestListItem;
