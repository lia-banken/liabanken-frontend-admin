import React, {FC, useContext, useEffect, useState} from "react";
import {makeStyles} from "@material-ui/core";
import {Button, TextField} from "@mui/material";
import SearchIcon from '@mui/icons-material/Search';
import DropDownFilterCity from "../dropDownFilter/DropDownFilterCity";
import DropDownFilterBransch from "../dropDownFilter/DropDownFilterBransch";
import SchoolListItem, {ISchoolItem} from "./SchoolListItem";

import API from "../../../route/API";
import SchoolProfile from "../../schoolProfile/SchoolProfile";
import {AdminApiContext} from "../../../App";
import {log} from "util";


const useStyles = makeStyles(() => ({
    header: {
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        flexDirection: "row",
        margin: "30px",

        "@media (max-width: 650px)": {
            fontSize: "18px",

        }
    },
    textFiledAndButton: {
        marginBottom: "30px"
    },
    input: {
        width: "80%",
        backgroundColor: 'white',
        border: 'none'
    },
    searchButton: {
        height: '55px',
        backgroundColor: "#081992",
        color: "#efeaea",
        "&:hover": {
            boxShadow: "none",
            backgroundColor: "#4c5499",
        },
        "@media (min-width: 800px)": {
            marginLeft: '0.5%'
        }
    },
    filterAction: {
        margin: "20px",
        display: 'flex',
        justifyContent: 'space-evenly',
        "@media (max-width: 500px)": {
            flexDirection: "column"
        }
    },

}));
const SchoolList: FC = () => {
    const classes = useStyles();
    const [schoolList, setSchoolist] = useState<ISchoolItem[]>([]);
    const [showListItem, setShowListItem] = useState(true)
    const [showSchoolProfile, setShowSchoolProfile] = useState(false)
    const [s, setS] = useState("")
    const adminApi = useContext(AdminApiContext);
    // const [school, setSchool] = useState("")
    const toggleViews = () => {
        if (showListItem == true) {
            setShowListItem(false)
            setShowSchoolProfile(true)
        } else if (showSchoolProfile == true) {
            setShowSchoolProfile(false)
            setShowListItem(true)
        }
    };

    useEffect(() => {
        const fetchData = async () => {
            await adminApi.fetchAllApprovedSchools().then(schools => setSchoolist(schools))
        };
        const timer = setTimeout(() => {
            fetchData();
        }, 1000);
        return () => clearTimeout(timer);
    }, []);

    const filterSchool = (e: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => {
        const tmpArray = schoolList.filter(item => item.schoolName.toLowerCase().includes(e.target.value))
        setSchoolist(tmpArray)
        if (e.target.value == "") {
            adminApi.fetchAllApprovedSchools().then(schools => setSchoolist(schools))
        }
    };

    const removeSchoolOnClick = async (id: string) => {
        await adminApi.deleteApproveSchool(id)
        await adminApi.fetchAllApprovedSchools().then(schools => setSchoolist(schools))
    };

    function setSchoolby(id: string) {
        return console.log("id" + id);
    }

    const SchoolListInfo = () => {
        return (
            <div>
                <h2 className={classes.header}>Mina Skolor</h2>
                <div className={classes.textFiledAndButton}>
                    <TextField
                        className={classes.input} id="search"
                        placeholder='Skriv in text...'
                        type="text"
                        onChange={e => filterSchool(e)}
                    />
                    <Button variant={'outlined'} className={classes.searchButton}><SearchIcon/>
                        Sök</Button>
                </div>
                <div className={classes.filterAction}>
                    <DropDownFilterCity/>
                    <DropDownFilterBransch/>
                </div>
                <div>
                    < SchoolListItem list={schoolList}
                                     onClick={toggleViews}
                                     sendSchoolId={(id) => removeSchoolOnClick(id)}
                                     SchoolId={(id) => setSchoolby(id)}
                    />
                </div>
            </div>
        );
    }
    return (
        <div>
            {showListItem && SchoolListInfo()}
            {showSchoolProfile && <SchoolProfile id={""}/>}
        </div>
    );
}
export default SchoolList;
