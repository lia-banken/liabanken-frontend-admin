import React, {FC, useState} from 'react';
import {Button, makeStyles} from "@material-ui/core";
import SchoolProfile, {params} from "../../schoolProfile/SchoolProfile";

const useStyles = makeStyles((theme) => ({
    title: {
        color: "#081992",
    },
    schoolItem: {
        border: "1px solid lightgrey",
        '&:hover': {
            color: "blue",
            cursor: "pointer",
            backgroundColor: "#e6eeff"
        }
    },
    buttonContainer: {
        margin: "10px"
    }
}))

export interface ISchoolItem {
    id: string
    email: string
    schoolName: string
    organizationNumber: string
    address: { city: string }
}

interface IOnClick {
    onClick: (id: string) => void
    sendSchoolId: (id: string) => void
    SchoolId: (id: string) => void
}

interface ISchoolList {
    list: ISchoolItem[]
}

const SchoolListItem: FC<ISchoolList & IOnClick> = (props) => {
    const classes = useStyles();
    const [schoolItemShow, setSchoolItemShow] = useState(true)
    const [schoolProfileShow, setSchoolProfileShow] = useState(false)
    const [schoolId, setSchoolId] = useState<string>('');
    const [s, setS] = useState<params>();

    const onClicked = (id:string) => {
        console.log("item ID: " + id)
        props.SchoolId(id)
        setSchoolId(id)
        setSchoolProfileShow(true)
        setSchoolItemShow(false)
    }

    const schoolItemInfo = () => {
        return (
            <div>
                {props.list.map((item) => (
                    <div onClick={() => props.onClick(item.id)}
                         className={classes.schoolItem}
                         key={item.id}
                    >
                        <h3 className={classes.title}>
                            {item.schoolName}
                        </h3>
                        <p>{item.organizationNumber}</p>
                        <p>{item.address.city}</p>
                        <div className={classes.buttonContainer}>
                            <Button
                                variant="contained"
                                color="primary"
                                onClick={(event) => {
                                    event.stopPropagation()
                                    props.sendSchoolId(item.id)
                                }}
                            >
                                Ta bort
                            </Button>
                        </div>
                    </div>
                ))}
            </div>
        );
    }

    return (
        <div>
            {schoolItemShow ? schoolItemInfo() : null }
         {/*    schoolProfileShow ? <SchoolProfile id={schoolId} /> : null*/}
        </div>
    );
}

export default SchoolListItem;
