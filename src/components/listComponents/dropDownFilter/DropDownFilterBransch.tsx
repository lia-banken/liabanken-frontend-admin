import React from 'react';
import {
    Box,
    createStyles,
    FormControl,
    InputBase,
    InputLabel,
    makeStyles,
    MenuItem,
    Select,

} from "@material-ui/core";
const useStyles = makeStyles((theme) => ({
    block:{
        display:'flex',
        width: '40%',
    },
    formControl: {
        backgroundColor:'#fff',
        width:'180px'
    },
    name:{
        marginLeft:'7px',
        marginTop:'-7px'
    }
}))
const DropDownFilterBransch = () => {
    const classes = useStyles();
    const [bransch, setBransch] = React.useState('');
    const handleChange = (event: React.ChangeEvent<{ value: unknown }>) => {
        setBransch(event.target.value as string);
    };
    return (
        <div><Box className={classes.block} ><FormControl className={classes.formControl}><InputLabel className={classes.name} id="demo-simple-select-label">Bransch</InputLabel><Select
            labelId="demo-simple-select-label"
            id="demo-simple-select"
            value={bransch}                        label="Bransch"
            onChange={handleChange}                    ><MenuItem value={''}>None</MenuItem><MenuItem value={10}>JavaDeveloper</MenuItem><MenuItem value={20}>IosDeveloper</MenuItem><MenuItem value={30}>WebDeveloper</MenuItem></Select></FormControl></Box></div>

    );
};
export default DropDownFilterBransch;