import React from 'react';
import {
    Box,
    createStyles,
    FormControl,
    InputBase,
    InputLabel,
    makeStyles,
    MenuItem,
    Select,

} from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
    block: {
        display: 'flex',
        width: '40%',
        "@media (max-width: 500px)": {
            paddingBottom: "20px",
            marginTop: "10px"
        }
    },
    formControl: {
        backgroundColor: '#fff',
        width: '180px'
    },
    name: {
        marginLeft: '7px',
        marginTop: '-7px'
    }
}))
const DropDownFilterCity = () => {
    const classes = useStyles();
    const [stad, setStad] = React.useState('');
    const handleChange = (event: React.ChangeEvent<{ value: unknown }>) => {
        setStad(event.target.value as string);
    };
    return (
        <div>
            <Box className={classes.block}>
                <FormControl className={classes.formControl}>
                    <InputLabel className={classes.name} id="demo-simple-select-label">Stad</InputLabel>
                    <Select
                        labelId="demo-simple-select-label"
                        id="demo-simple-select"
                        value={stad}
                        label="Stad"
                        onChange={handleChange}
                    >
                        <MenuItem value={''}>None</MenuItem>
                        <MenuItem value={10}>Stockholm</MenuItem>
                        <MenuItem value={20}>Malmö</MenuItem>
                        <MenuItem value={30}>Örebro</MenuItem>
                    </Select>
                </FormControl>
            </Box>
            <div>

            </div>

        </div>

    );
};
export default DropDownFilterCity;
