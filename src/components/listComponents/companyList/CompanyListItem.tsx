import React from 'react';
import {Button, makeStyles} from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
    title: {
        color: "#081992",
    },
    companyItem: {
        border: "1px solid lightgrey",
    },
    buttonContainer: {
        margin: "10px"
    }
}))

export interface ICompanyItem {
    id: string
    email: string
    companyName: string
    address: { city: string }
    searchText: string
}

interface IStudentList {
    list: ICompanyItem[]
}

const CompanyListItem: React.FC<IStudentList> = ({list}) => {
    const classes = useStyles();
    return (
        <div>
            {list.map((item) => (
                <div className={classes.companyItem}>
                    <h3 className={classes.title}>{item.companyName}</h3>
                    <p>{item.address.city}</p>
                    <p>{item.email}</p>
                    <div className={classes.buttonContainer}>
                    </div>
                </div>
            ))}
        </div>
    );
};

export default CompanyListItem;