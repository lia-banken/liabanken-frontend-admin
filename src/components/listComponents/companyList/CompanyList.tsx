import React, {FC, useEffect, useState} from "react";
import {makeStyles} from "@material-ui/core";
import {Button, TextField} from "@mui/material";
import SearchIcon from '@mui/icons-material/Search';
import DropDownFilterCity from "../dropDownFilter/DropDownFilterCity";
import DropDownFilterBransch from "../dropDownFilter/DropDownFilterBransch";
import CompanyListItem, {ICompanyItem} from "./CompanyListItem";
import API from '../../../route/API';


const useStyles = makeStyles(() => ({

    header: {
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        flexDirection: "row",
        margin: "30px",
        "@media (max-width: 650px)": {
            fontSize: "18px",

        }

    },
    input: {
        width: "80%",
        backgroundColor: 'white',
        border: 'none'
    },
    searchButton: {
        height: '55px',
        backgroundColor: "#081992",
        color: "#efeaea",
        "&:hover": {
            boxShadow: "none",
            backgroundColor: "#4c5499",
        },
        "@media (min-width: 800px)": {
            marginLeft: '0.5%'
        }
    },
    filterAction: {
        display: 'flex',
        justifyContent: 'space-evenly',
        "@media (max-width: 500px)": {
            flexDirection: "column"
        }
    },
}));

const CompanyList: FC = () => {
    const classes = useStyles();
    const [companyList, setCompanyList] = useState<ICompanyItem[]>([]);

   /* useEffect(() => {
        const fetchData = async () => {
            await API.fetchAllCompanies().then(companies => setCompanyList(companies))
        };
        const timer = setTimeout(() => {
            fetchData();
        }, 1000);
        return () => clearTimeout(timer);
    }, []);*/

/*    const filterCompany = (e: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => {
        const tmpArray = companyList.filter(item => item.companyName.toLowerCase().includes(e.target.value))
        setCompanyList(tmpArray)
        if (e.target.value == "") {
            API.fetchAllCompanies().then(companies => setCompanyList(companies))
        }
    };*/

    return (
        <div>
            <div>
                <h2 className={classes.header}>Mina Företag</h2>
                <div>
                    <TextField
                        className={classes.input}
                        id="search"
                        placeholder='Skriv in text...'
                        type="text"
                        //onChange={e => filterCompany(e)}
                    />
                    <Button
                        variant={'outlined'}
                        className={classes.searchButton}>
                        <SearchIcon/>
                        Sök
                    </Button>
                </div>
                <div className={classes.filterAction}>
                    <DropDownFilterCity/>
                    <DropDownFilterBransch/>
                </div>
            </div>
            <div>
                <CompanyListItem list={companyList}/>
            </div>
        </div>
    );
}
export default CompanyList;
