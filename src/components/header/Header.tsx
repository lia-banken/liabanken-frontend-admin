import {makeStyles} from '@material-ui/styles/';
import React, {useState} from 'react';
import {Button, Divider, Link} from "@mui/material";
import SearchBar from "./SearchBar";
import {useHistory} from "react-router-dom";
import RightMenu from "./RightMenu";
import {useKeycloak} from "@react-keycloak/web";
import HeaderButton from "./HeaderButton";


const useStyles = makeStyles(() => ({
    header: {
        display: "flex",
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-evenly",
        maxWidth: "100%",
        margin: "auto",
        "@media (max-width: 650px)": {
            width: "100%",
            border: "1px solid lightgray",
            display: "flex",
            flexDirection: "column",
            justifyContent: "center",
            alignItems: "center",
            marginRight: "auto",
            fontSize: "25px",
            paddingTop: "20px"
        }
    },
    liabanken: {
        /*margin: "10px"*/
    },
    loggaut: {
        width: "150px",
        height: "40px"
    },
    liabankenText: {width: "150px", height: "40px"},
    rightheader: {
        marginTop: "10px",
        width: "90%",
        display: "flex",
        flexDirection: "row",
        justifyContent: "end",
        "@media (max-width: 650px)": {
            display: "flex",
            flexDirection: "column",
            justifyContent: "center",
            alignItems: "center",
        }
    },
    searchBar: {
        "@media (max-width: 650px)": {
            display: "none",
        }
    }

}));


const Header: React.FC = () => {
    const history = useHistory()
    const [clicked, setClicked] = useState<boolean>(false);
    const [buttonText, setButtonText] = useState<string>("Logga in");
    const classes = useStyles();
    const [isOpen, setIsOpen] = useState(false);
    const {keycloak, initialized} = useKeycloak()
    const btnHandler = () => {
        setIsOpen(true)
    };
    console.log("text keycloak", keycloak.token)
    /*  const handleLogIn = () => {
          if (!clicked) {
              history.push("/admin/LoggaInPage")
              setButtonText("Logga ut")
              setClicked(true);
          } else {
              history.push("/")
              setButtonText("Logga in")
              setClicked(false);
          }
      }*/

    /*   {keycloak.authenticated ?
           <HeaderButton text={"Logga out"} onClick={() => keycloak.logout()} rout={"/student/login"} /> :
           <HeaderButton text={"Logga In"} onClick={() => keycloak.login()}/>
       }*/
    return (
        <div className={classes.header}>
            <Link href="/" data-test='header-link' underline="none" style={{fontSize: "20px", fontWeight: "bold"}}>
                LiaBanken
            </Link>
            <div className={classes.rightheader}>
                {keycloak.authenticated ?
                    <HeaderButton text={"Logga out"} onClick={() => keycloak.logout()} rout={"/student/login"}/> :
                    <HeaderButton text={"Logga In"} onClick={() => keycloak.login()}/>
                }
                <HeaderButton text={"Sök"} rout={"/"}/>
                <HeaderButton text={"Menu"} onClick={() => btnHandler()}>Menu</HeaderButton>
                {/*  <div className={classes.searchBar}><SearchBar/> </div>*/}
                <RightMenu/>
            </div>
        </div>
    );
}

export default Header;
