import * as React from 'react';
import Box from '@mui/material/Box';
import Drawer from '@mui/material/Drawer';
import Button from '@mui/material/Button';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import MenuIcon from '@mui/icons-material/Menu';
import AccountBoxIcon from '@mui/icons-material/AccountBox';
import HomeIcon from '@mui/icons-material/Home';
import ManageSearchIcon from '@mui/icons-material/ManageSearch';
import {useHistory} from "react-router-dom";
import {makeStyles} from "@material-ui/styles";


type Anchor = 'right';

const useStyles = makeStyles(() => ({
    list: {
        width: "300px",
    },
    listItem: {
        display: "flex",
        flexDirection: "row",
        alignItems: "center",
        '&:hover': {
            color: "blue",
            cursor: "pointer"
        }
    }

}));

export default function RightMenu() {
    const [state, setState] = React.useState({right: false});
    const classes = useStyles();
    const history = useHistory()
    const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);

    const toggleDrawer =
        (anchor: Anchor, open: boolean) =>
            (event: React.KeyboardEvent | React.MouseEvent) => {
                if (
                    event.type === 'keydown' &&
                    ((event as React.KeyboardEvent).key === 'Tab' ||
                        (event as React.KeyboardEvent).key === 'Shift')
                ) {
                    return;
                }
                setState({...state, [anchor]: open});
            };

    const navigateToLandingAndClose = (text: string) => {
        switch (text) {
            case"Start":
                return history.push('/')
            case"Start":
                return history.push('/')
            case"Mina Sidor":
                return history.push('/admin/minaSidor')
            case"Sök":
                return history.push('/')
        }

    };

    const calculateIcon = (text: string) => {
        switch (text) {
            case"Start":
                return <HomeIcon/>
            case"Mina Sidor":
                return <AccountBoxIcon/>
            case"Sök":
                return <ManageSearchIcon/>
        }
    };

    const list = (anchor: Anchor) => (
        <Box
            role="presentation"
            onClick={toggleDrawer(anchor, false)}
            onKeyDown={toggleDrawer(anchor, false)}
            className={classes.list}
        >
            <List>
                {['Start', 'Mina Sidor', 'Sök'].map((text, index) => (
                    <ListItem className={classes.listItem} button key={text} onClick={() => navigateToLandingAndClose(text)}>
                        <ListItemIcon>
                            {calculateIcon(text)}
                        </ListItemIcon>
                        <ListItemText primary={text}/>
                    </ListItem>
                ))}
            </List>
        </Box>
    );

    return (
        <div>
            {(['right'] as const).map((anchor) => (
                <React.Fragment key={anchor}>
                    <Button
                        data-testid="menuBtn"
                        onClick={toggleDrawer(anchor, true)}
                    >
                        {<MenuIcon style={{width: "50px", height: "50px"}}/>}
                    </Button>
                    <Drawer
                        anchor={anchor}
                        open={state[anchor]}
                        onClose={toggleDrawer(anchor, false)}
                    >
                        {list(anchor)}
                    </Drawer>
                </React.Fragment>
            ))}
        </div>
    );
}