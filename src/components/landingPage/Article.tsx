import React from 'react';
import {makeStyles} from "@material-ui/core";
import {Card, CardActionArea, CardContent, CardMedia, Typography} from "@mui/material";

const useStyles = makeStyles(() => ({
    article: {
        border: "1px solid #081992",
        marginBottom: "10px",
        "@media (min-width: 1200px)": {
            width: "350px",
        },
    },
    container: {
     /*   display: "flex",
        justifyContent: "center",*/
    },
}));

interface IArticle {
    titleText: string;
    text: string;
}

const Article: React.FC<IArticle> = ({titleText, text}) => {
    const classes = useStyles();
    return (
        <div className={classes.container}>
            <Card className={classes.article}>
                <CardActionArea>
                    <CardContent>
                        <Typography gutterBottom variant="h5" component="div">
                            {titleText}
                        </Typography>
                        <Typography variant="body2" color="text.secondary">
                            {text}
                        </Typography>
                    </CardContent>
                </CardActionArea>
            </Card>
        </div>
    );
};

export default Article;
