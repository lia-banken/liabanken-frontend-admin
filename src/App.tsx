import React, {createContext} from 'react';
import Header from "./components/header/Header";
import {BrowserRouter} from "react-router-dom";
import {makeStyles} from "@material-ui/styles";
import Footer from "./components/footer/Footer";
import Routes from "./route/Routes";
import keycloak from "./keycloak";
import { ReactKeycloakProvider } from '@react-keycloak/web'
import API from "./route/API";

export const AdminApiContext = createContext(new API(null));
const useStyles = makeStyles(() => ({
    root: {
        background: "linear-gradient(0deg, #1ecfec 5%, rgba(0, 0, 0, 0) 90%)"
    }
}));

function App() {
    const classes = useStyles();
    return (
        <div className={classes.root}>
        <BrowserRouter >
            <ReactKeycloakProvider  authClient={keycloak}>
                <AdminApiContext.Provider value={new API(keycloak)}>
            <Header/>
            <Routes/>
            <Footer/>
                </AdminApiContext.Provider>
            </ReactKeycloakProvider>
        </BrowserRouter>
        </div>
    );
}

export default App;
